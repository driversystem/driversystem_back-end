const mongoose = require('mongoose');
const GeoSchema = require('./schemas/geo');

const Collection = mongoose.model(
    "collection",
    new mongoose.Schema({
        collectionName: String,
        description: String,
        locations:[{
            geo: GeoSchema
        }],
    }),
);

module.exports = Collection;
