const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GeoSchema = require('./schemas/geo');

const RescueCenterRequestSchema = new Schema({
	userID: {
		type: Schema.Types.ObjectId,
		ref: 'user',
	},
	location: GeoSchema,
	description: String,
	images: [],
	time: Date,
	audio: String,
	status: String,
});

const RescueCenterRequest = mongoose.model('rescueCenterRequest', RescueCenterRequestSchema);

module.exports = RescueCenterRequest;
