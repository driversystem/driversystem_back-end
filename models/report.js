const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const GeoSchema = require('./schemas/geo');

const ReportSchema = new Schema({
	type: {
		type: String,
		require: true,
	},
	description: {
		type: String,
		default: '',
	},
	geometry: GeoSchema,
	userID: {
		type: Schema.Types.ObjectId,
		ref: 'user',
	},
	numReport: Number,
	numDelete: Number,
	status: Boolean,
	image: {
		type: String,
		default: '',
	},
	audio: {
		type: String,
		default: '',
	},
	phoneNumber: String,
	expiresAt: {
		type: Date,
		expires: 0,
		default: moment().add(30, 'minutes').toDate(), // defaults to 30 minutes after creation
	},
});

const Report = mongoose.model('report', ReportSchema);

module.exports = Report;
