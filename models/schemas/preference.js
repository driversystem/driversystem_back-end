const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PreferenceSchema = new Schema(
	{
		driverScanRadius: {
			type: Number,
			required: true,
			default: 500,
		},
		reportScanRadius: {
			type: Number,
			required: true,
			default: 500,
		},
		locationScanRadius: {
			type: Number,
			required: true,
			default: 500,
		},
	},
	{ _id: false }
);

module.exports = PreferenceSchema;
