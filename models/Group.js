const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Group = mongoose.model(
	'group',
	new mongoose.Schema({
		groupName: String,
		avatar: String,
		description: String,
		groupOwner: {
			type: Schema.Types.ObjectId,
			ref: 'user',
		},
		groupModerators: [
			{
				type: Schema.Types.ObjectId,
				ref: 'user',
			},
		],
		members: [
			{
				type: Schema.Types.ObjectId,
				ref: 'user',
			},
		],
		appointments: [
			{
				type: Schema.Types.ObjectId,
				ref: 'appointment',
			},
		],
		pendingInvitations: [
			{
				type: Schema.Types.ObjectId,
				ref: 'user',
			},
		],
		photos: [
			{
				type: String,
			}
		]
	})
);

module.exports = Group;
