const mongoose = require('mongoose');
const GeoSchema = require('./schemas/geo');

const Appointment = mongoose.model(
	'appointment',
	new mongoose.Schema({
		time: Date,
		group: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'group',
		},
		reminderTime: Date,
		reminderMsg: String,
		location: GeoSchema,
		locationName: String,
		locationAddress: String,
		accepted: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'user',
			},
		],
		properties: {
			groupTravel: {
				type: Boolean,
				default: false,
			},
		},
		status: String,
		endedAt: {
			type: Date,
			expires: 3600, // document expires 1 hour after appointment ended.
		},
	})
);

module.exports = Appointment;
