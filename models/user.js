const mongoose = require('mongoose');
const GeoSchema = require('./schemas/geo');
const PreferenceSchema = require('./schemas/preference');

const bcrypt = require('bcrypt'),
	SALT_WORK_FACTOR = 10;
const mongoose_fuzzy_searching = require('mongoose-fuzzy-searching');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	email: {
		type: String,
		trim: true,
		index: true,
		unique: true,
		sparse: true,
	},
	password: String,
	name: {
		type: String,
		default: 'Doe',
		required: true,
	},
	role: {
		type: String,
		default: 'User',
		required: true,
	},
	birthDate: Date,
	authProvider: {
		type: String,
		default: 'SMUser',
		required: true,
	},
	providerUID: {
		type: String,
		default: 'uidPlaceHolder@supermap',
		required: true,
	},
	avatar: String,
	socketID: String,
	socketIsActive: {
		type: Boolean,
		default: false,
	},
	currentLocation: GeoSchema,
	latHomeLocation: Number,
	longHomeLocation: Number,
	latWorkLocation: Number,
	longWorkLocation: Number,
	invisible: {
		type: Boolean,
		required: true,
		default: false,
	},
	location: {
		type: String,
		default: null,
	},
	phoneNumber: {
		type: String,
		trim: true,
		index: true,
		unique: true,
		sparse: true,
	},
	vehicles: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'vehicle',
		},
	],
	groups: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'group',
		},
	],
	favouriteGroups: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'group',
		},
	],
	pendingInvitations: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'group',
		},
	],
	collections: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'collection',
		},
	],
	preferences: {
		type: PreferenceSchema,
		default: {
			driverScanRadius: 500,
			reportScanRadius: 500,
			locationScanRadius: 500,
		},
	},
	acceptedAppointments: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'appointment',
		},
	],
	activated: {
		type: Boolean,
		default: false,
	},
	utilToken: String,
	utilTokenExpires: Date,
});

UserSchema.plugin(mongoose_fuzzy_searching, {
	fields: [ 'name', 'email' ],
});

// Method to compare password for login
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
		if (err) return cb(err);
		cb(null, isMatch);
	});
};

// Hash password
UserSchema.methods.hashPassword = function() {
	let salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);
	this.password = bcrypt.hashSync(this.password, salt);
};

UserSchema.pre('save', function(next) {
	let user = this;

	if (user.authProvider !== 'SMUser') return next();

	// only hash the password if it hasn't been modified (or is new)
	if (!user.isModified('password')) return next();

	// generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
		if (err) return next(err);

		// hash the password using our new salt
		bcrypt.hash(user.password, salt, function(err, hash) {
			if (err) return next(err);

			// override the cleartext password with the hashed one
			user.password = hash;
			next();
		});
	});
});

module.exports = mongoose.model('user', UserSchema);
