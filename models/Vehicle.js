const mongoose = require('mongoose');

const VehicleSchema = new mongoose.Schema({
	vehicleType: String,
	vehicleModel: String,
	color: String,
	licensePlate: {
		type: String,
		default: '',
	},
});

const Vehicle = mongoose.model('vehicle', VehicleSchema);

module.exports = Vehicle;
