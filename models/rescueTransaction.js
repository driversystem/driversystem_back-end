const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RescueTransactionSchema = new Schema({
	request: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'rescueCenterRequest',
	},
	rescueCenter: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'user',
	},
	status: {
		type: String,
		default: 'Pending',
	},
});

const RescueTransaction = mongoose.model('rescueTransaction', RescueTransactionSchema);

module.exports = RescueTransaction;
