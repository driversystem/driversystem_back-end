const express = require('express');
const User = require('../../../models/user');
const Vehicle = require('../../../models/Vehicle');
const Group = require('../../../models/Group');
const router = express.Router();
const mongoose = require('mongoose');
const validator = require('express-validator');

// Require authenticate
const { authenticate } = require('./middleware');

// Utilities
const { removeSensitiveUserData, dupe } = require('./utils');
const { remove } = require('../../../models/schemas/geo');
const user = require('../../../models/user');

// Api for users
router.get('/profile', authenticate, (req, res, next) => {
	let userID = req.decoded.userID;
	if (!userID) {
		return res.status(422).json({
			success: false,
			message: 'Không thể truy cập userID',
		});
	}

	User.findById(userID)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}

			return res.status(200).json({
				success: true,
				user: removeSensitiveUserData(user),
			});
		})
		.catch(next);
});

// Get all user
router.get('/', authenticate, (req, res, next) => {
	User.find({})
		.then((users) => {
			users = users.map((user) => removeSensitiveUserData(user));
			return res.status(200).json({
				success: true,
				users,
			});
		})
		.catch(next);
});

// Update current location of user
router.put('/location/current', authenticate, (req, res, next) => {
	let userID = req.decoded.userID;

	if (!userID) {
		return res.status(422).json({
			success: false,
			message: 'Không thể truy cập userID',
		});
	}

	User.findByIdAndUpdate(
		userID,
		{
			currentLocation: req.body.currentLocation,
		},
		{ new: true }
	)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}
			return res.status(200).json({
				success: true,
				user: removeSensitiveUserData(user),
			});
		})
		.catch(next);
});

// Update socket ID
router.put('/updateSocketID', authenticate, (req, res, next) => {
	let userID = req.decoded.userID;
	if (!userID) {
		return res.status(422).json({
			success: false,
			message: 'Không thể truy cập userID',
		});
	}

	User.findByIdAndUpdate(
		userID,
		{
			socketID: req.body.socketID,
		},
		{ new: true }
	)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}
			return res.status(200).json({
				success: true,
				user: removeSensitiveUserData(user),
			});
		})
		.catch(next);
});

// Get all vehicles of user
router.get('/vehicle', authenticate, (req, res, next) => {
	let userID = req.decoded.userID;

	if (!userID) {
		return res.status(422).json({
			success: false,
			message: 'Không thể truy cập userID',
		});
	}

	User.findById(userID)
		.populate({
			path: 'vehicles',
		})
		.then((user) => {
			return res.status(200).json({
				success: true,
				vehicles: user.vehicles,
			});
		})
		.catch(next);
});

// Greeting other vehicle
router.get('/greeting', (req, res, next) => {
	res.sendFile(__dirname + '' + '/index.html');
});

router.get('/nearby', authenticate, (req, res, next) => {
	var userID = req.decoded.userID;

	console.log(req.query);
	var lat = parseFloat(req.query.lat);
	var lng = parseFloat(req.query.lng);
	var radius = parseFloat(req.query.radius) || 300; //in meters

	if ((!lng && lng !== 0) || (!lat && lat !== 0)) {
		return res.status(422).json({
			success: false,
			message: 'Yêu cầu tọa độ hiện tại với các tham số lng, lat',
		});
	}

	var geoOptions = {
		near: {
			type: 'Point',
			coordinates: [ lng, lat ],
		},
		spherical: true,
		distanceField: 'distance',
		maxDistance: radius,
	};

	console.log('Nearby Location: ' + lat + '; ' + lng + ' :: radius: ' + radius);

	User.aggregate()
		.near(geoOptions)
		.then((results) => {
			console.log(`Results: ${JSON.stringify(results, null, 2)}`);
			var users = [];
			results.forEach((result) => {
				// Exclude current user and invisible users
				if (result._id != userID && !result.invisible) {
					users.push(removeSensitiveUserData(result));
				}
			});

			return res.status(200).json({
				success: true,
				users,
			});
		})
		.catch(next);
});

// Update home location
router.put('/location/home', authenticate, (req, res, next) => {
	let userID = req.decoded.userID;

	if (!userID) {
		return res.status(422).json({
			success: false,
			message: 'Không thể truy cập userID',
		});
	}

	User.findByIdAndUpdate(
		userID,
		{
			latHomeLocation: req.body.latHomeLocation,
			longHomeLocation: req.body.longHomeLocation,
		},
		{ new: true }
	)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}
			return res.status(200).json({
				success: true,
				user: removeSensitiveUserData(user),
			});
		})
		.catch(next);
});

// Update work location
router.put('/location/work', authenticate, (req, res, next) => {
	let userID = req.decoded.userID;

	if (!userID) {
		return res.status(422).json({
			success: false,
			message: 'Không thể truy cập userID',
		});
	}

	User.findByIdAndUpdate(
		userID,
		{
			latWorkLocation: req.body.latWorkLocation,
			longWorkLocation: req.body.longWorkLocation,
		},
		{ new: true }
	)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}
			return res.status(200).json({
				success: true,
				user: removeSensitiveUserData(user),
			});
		})
		.catch(next);
});

// Gets all groups of user
router.get('/groups', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	if (!mongoose.Types.ObjectId.isValid(userID)) {
		return res.status(400).json({
			success: false,
			message: 'User ID không hợp lệ',
		});
	}

	User.findOne({ _id: userID }, 'groups')
		.populate({
			path: 'groups',
			select: 'groupName avatar description _id',
		})
		.then((user) => {
			if (!user) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			const groups = user.groups;
			return res.status(200).json({
				success: true,
				groups,
			});
		})
		.catch(next);
});

// Get all groups that user owns
router.get('/groups/isOwner', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	if (!mongoose.Types.ObjectId.isValid(userID)) {
		return res.status(400).json({
			success: false,
			message: 'User ID không hợp lệ',
		});
	}

	User.findOne({ _id: userID }, 'groups')
		.populate({
			path: 'groups',
			select: 'groupName avatar _id',
			match: {
				$or: [ { groupOwner: userID }, { groupModerators: userID } ],
			},
		})
		.then((user) => {
			if (!user) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			const groups = user.groups;
			return res.status(200).json({
				success: true,
				groups,
			});
		})
		.catch(next);
});

// Gets all appointments of a user
router.get('/appointments', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	if (!mongoose.Types.ObjectId.isValid(userID)) {
		return res.status(400).json({
			success: false,
			message: 'User ID không hợp lệ',
		});
	}

	User.findById(userID, 'groups')
		.populate({
			path: 'groups',
			select: '_id name appointments',
			populate: {
				path: 'appointments',
				populate: {
					path: 'accepted',
					select: '_id name email',
				},
			},
		})
		.then((user) => {
			if (!user) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			let appointments = [];
			user.groups.forEach((group) => {
				group.appointments.forEach((appointment) => {
					appointments.push(appointment);
				});
			});

			return res.status(200).json({
				success: true,
				appointments,
			});
		})
		.catch(next);
});

// Get user by phone number
router.get('/phone', authenticate, (req, res, next) => {
	const phoneNumber = req.query.numbers;

	User.findOne({ phoneNumber: phoneNumber })
		.then((user) => {
			if (!user) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy người dùng',
				});
			}

			return res.status(200).json({
				success: true,
				user: removeSensitiveUserData(user),
			});
		})
		.catch(next);
});

// Dùng cho màn hình điền thông tin user
router.put('/profile', authenticate, (req, res, next) => {
	req.checkBody({
		name: {
			notEmpty: true,
			errorMessage: 'name required.',
		},
		phoneNumber: {
			notEmpty: true,
			errorMessage: 'name required',
		},
	});

	let id = req.decoded.userID;
	const patch = {};
	if (req.body.avatar) patch.avatar = req.body.avatar;
	if (req.body.name) patch.name = req.body.name;
	if (req.body.phoneNumber) patch.phoneNumber = req.body.phoneNumber;
	if (req.body.homeLat) patch.homeLat = req.body.homeLat;
	if (req.body.homeLng) patch.homeLng = req.body.homeLng;
	if (req.body.workLat) patch.workLat = req.body.workLat;
	if (req.body.workLng) patch.workLng = req.body.workLng;

	User.findByIdAndUpdate(id, patch, { new: true })
		.then((user) => {
			if (!user) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			return res.status(200).json({
				success: true,
				user: user,
			});
		})
		.catch((err) => {
			return res.status(500).json({
				success: false,
				message: 'Lỗi không xác định!!!',
				err,
			});
		})
		.catch(next);
});

// Get all invitations
router.get('/invitation', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	User.findById(userID)
		.select('pendingInvitations')
		.populate({
			path: 'pendingInvitations',
			select: 'groupName avatar groupOwner',
			populate: {
				path: 'groupOwner',
				select: 'name',
			},
		})
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			return res.status(200).json({
				success: true,
				invitations: user.pendingInvitations,
			});
		});
});

// Đông ý invitation
router.put('/invitation/accept', authenticate, (req, res, next) => {
	const groupID = req.query.id;
	const userID = req.decoded.userID;

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Group ID không hợp lệ',
		});
	}

	// First, check if invitation exists
	User.findById(userID)
		.select('pendingInvitations')
		.then((user) => {
			if (!user.pendingInvitations.includes(groupID)) {
				return res.status(404).json({
					success: false,
					message: 'Invitation does not exist',
				});
			}

			// If invitation exists, accept it by
			// 1. Add user to group's members list
			// 2. Remove member's ID from group's pendingInvitations list
			// 3. Remove group ID from user's pendingInvitations list

			// (1. + 2.)
			Group.findByIdAndUpdate(
				groupID,
				{
					$addToSet: { members: userID },
					$pull: { pendingInvitations: userID },
				},
				{ new: true }
			)
				.then((group) => {
					if (!group) {
						return res.status(404).json({
							success: false,
							message: 'Không tìm thấy group',
						});
					}

					const groupObj = {
						_id: group._id,
						groupName: group.groupName,
						avatar: group.avatar,
					};

					// (3.)
					User.findByIdAndUpdate(
						userID,
						{
							$pull: { pendingInvitations: groupID },
							$addToSet: { groups: groupID },
						},
						{ new: true }
					)
						.then((user) => {
							if (!user) {
								return res.status(404).json({
									success: false,
									message: 'Không tìm thấy user',
								});
							}

							return res.status(200).json({
								success: true,
								message: 'Successfully accepted invitation',
								user: removeSensitiveUserData(user),
								group: groupObj,
							});
						})
						.catch(next);
				})
				.catch(next);
		})
		.catch(next);
});

// Từ chối gia nhập nhóm
router.put('/invitation/deny', authenticate, (req, res, next) => {
	const groupID = req.query.id;
	const userID = req.decoded.userID;

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Group ID không hợp lệ',
		});
	}

	// First, check if invitation exists
	console.log(userID);
	User.findById(userID).select('pendingInvitations').then((user) => {
		console.log(`User: ${user}`);
		if (!user.pendingInvitations.includes(groupID)) {
			return res.status(404).json({
				success: false,
				message: 'Invitation does not exist',
			});
		}

		// If invitation exists, deny it by
		// 1. Remove user's ID from group's pendingInvitations list
		// 2. Removing group's ID from user's pendingInvitations list

		// (1.)
		Group.findByIdAndUpdate(groupID, {
			$pull: { pendingInvitations: userID },
		}).then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy group',
				});
			}

			// (2.)
			User.findByIdAndUpdate(userID, {
				$pull: { pendingInvitations: groupID },
			}).then((user) => {
				if (!user) {
					return res.status(404).json({
						success: false,
						message: 'Không tìm thấy user',
					});
				}

				return res.status(200).json({
					success: true,
					message: 'Successfully denied invitation',
					user: removeSensitiveUserData(user),
				});
			});
		});
	});
});

router.get('/preferences', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	User.findById(userID)
		.select('-_id invisible socketIsActive preferences')
		.then((result) => {
			if (!result) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			result = result.toObject();
			const { socketIsActive, invisible, preferences } = result;
			const prefs = { socketIsActive, invisible, ...preferences };

			return res.status(200).json({
				success: true,
				preferences: prefs,
			});
		})
		.catch(next);
});

router.put('/preferences', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	const invisible = req.body.invisible;
	const socketIsActive = req.body.socketIsActive;
	const { driverScanRadius, reportScanRadius, locationScanRadius } = req.body;
	const preferences = {
		driverScanRadius,
		reportScanRadius,
		locationScanRadius,
	};

	User.findByIdAndUpdate(
		userID,
		{
			invisible: invisible,
			socketIsActive: socketIsActive,
			preferences: preferences,
		},
		{ new: true }
	).then((user) => {
		if (!user) {
			return res.status(404).json({
				success: false,
				message: 'Không tìm thấy user',
			});
		}

		const { socketIsActive, invisible, preferences } = user;
		const result = { socketIsActive, invisible, preferences };

		res.status(200).json({
			success: true,
			preferences: result,
		});
	});
});

router.get('/search', (req, res, next) => {
	let q = req.query.q;
	let groupID = req.query.excludeGroup || null;
	let additionalQueries = {};
	if (groupID !== null) {
		additionalQueries.groups = {
			$ne: groupID,
		};
	}
	User.fuzzySearch(q, additionalQueries).select('name email').then((users) => {
		if (!users) {
			return res.status(404).json({
				success: false,
				message: 'Không tìm thấy user',
			});
		}

		//users = users.map((user) => removeSensitiveUserData(user));
		res.status(200).json({
			success: true,
			users,
		});
	});
});

// Thêm nhóm vào favorite
router.post('/group/favourite/:id', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;
	const groupID = req.params.id;

	User.findByIdAndUpdate(userID, {
		$addToSet: { favouriteGroups: groupID },
	})
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy người dùng',
				});
			}

			const groups = user.favouriteGroups;
			return res.status(200).json({
				success: true,
				groups: groups,
			});
		})
		.catch(next);
});

//  Xóa nhóm khỏi favorite
router.post('/group/unfavourite/:id', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;
	const groupID = req.params.id;

	User.findByIdAndUpdate(userID, {
		$pull: { favouriteGroups: groupID },
	})
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy người dùng',
				});
			}

			const groups = user.favouriteGroups;
			return res.status(200).json({
				success: true,
				groups: groups,
			});
		})
		.catch(next);
});

router.get('/appointments/accepted', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;

	User.findById(userID, 'acceptedAppointments')
		.populate({
			path: 'acceptedAppointments',
			populate: {
				path: 'accepted',
				select: '_id name email',
			},
		})
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm được người dùng này',
				});
			}

			return res.status(200).json({
				success: true,
				user,
			});
		})
		.catch(next);
});

router.post('/profile/picture', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;
	const avatar = req.body.avatar;

	User.findByIdAndUpdate(
		userID,
		{
			avatar: avatar,
		},
		{ new: true }
	)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: true,
					message: 'Không tìm thấy user',
				});
			}

			return res.status(200).json({
				success: true,
				user,
			});
		})
		.catch(next);
});

router.post('/profile/phone', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;
	const phone = req.body.phone;

	if (phone.length < 8) {
		return res.status(422).json({
			success: false,
			message: 'Số điện thoại không hợp lệ (chuỗi quá ngắn)',
		});
	}

	User.find({ phoneNumber: phone })
		.then((user) => {
			if (user && user.length > 0) {
				return res.status(403).json({
					success: false,
					message: 'Số điện thoại đã tồn tại!!!',
				});
			} else {
				User.findByIdAndUpdate(
					userID,
					{
						phoneNumber: phone,
					},
					{ new: true }
				)
					.then((user) => {
						if (!user) {
							return res.status(404).json({
								success: true,
								message: 'Không tìm thấy user',
							});
						}

						return res.status(200).json({
							success: true,
							user,
						});
					})
					.catch(next);
			}
		})
		.catch(next);
});

router.post('/profile/name', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;
	const name = req.body.name;

	User.findByIdAndUpdate(
		userID,
		{
			name: name,
		},
		{ new: true }
	)
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: true,
					message: 'Không tìm thấy user',
				});
			}

			return res.status(200).json({
				success: true,
				user,
			});
		})
		.catch(next);
});

module.exports = router;
