const express = require('express');
const User = require('../../../models/user');
const router = express.Router();
const jwt = require('jsonwebtoken');
const request = require('request');

// Utilities
const { removeSensitiveUserData } = require('./utils');

// Auth with google 3rd party provider
router.post('/provider_auth', (req, res, next) => {
	const providerUID = req.body.providerUID;

	User.findOne({ providerUID: providerUID })
		.then((user) => {
			if (user) {
				let token = jwt.sign({ userID: user._id, role: user.role }, process.env.secret, {
					expiresIn: process.env.tokenExpiresIn,
				});

				// return the information including token as JSON
				return res.status(200).json({
					success: true,
					message: 'Đăng nhập thành công',
					token: token,
					user: removeSensitiveUserData(user),
				});
			} else {
				console.log('Creating new user object for oAuth login');
				let newUser = new User({
					authProvider: req.body.authProvider,
					providerUID: req.body.providerUID,
					name: req.body.name,
					activated: true,
					role: 'User',
				});

				newUser
					.save()
					.then((user) => {
						let token = jwt.sign({ userID: user._id, role: user.role }, process.env.secret, {
							expiresIn: process.env.tokenExpiresIn,
						});

						return res.status(200).json({
							success: true,
							token: token,
							message: 'Người dùng mới, tạo profile',
							user: removeSensitiveUserData(user),
						});
					})
					.catch(next);
			}
		})
		.catch(next);
});

// auth with email
router.post('/email', (req, res, next) => {
	let email = req.body.email;
	let password = req.body.password;

	if (!email || !password) {
		return res.status(400).json({
			success: false,
			message: 'Email hoặc password không hợp lệ',
		});
	}

	User.findOne({
		email: email,
		authProvider: 'SMUser',
	})
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}

			user.comparePassword(password, (err, isMatch) => {
				if (err) {
					return res.status(500).json({
						success: false,
						message: 'Internal error',
						errors: err,
					});
				}

				if (!isMatch) {
					return res.status(401).json({
						success: false,
						message: 'Sai mật khẩu!',
					});
				}

				if (user.activated == false) {
					return res.status(401).json({
						success: false,
						message: 'Tài khoản chưa được kích hoạt!',
					});
				}

				let token = jwt.sign({ userID: user._id, role: user.role }, process.env.secret, {
					expiresIn: process.env.tokenExpiresIn,
				});

				// return the information including token as JSON
				return res.status(200).json({
					success: true,
					message: 'Đăng nhập thành công',
					token: token,
					user: removeSensitiveUserData(user),
				});
			});
		})
		.catch(next);
});

// Refresh token
router.post('/refresh_token', (req, res, next) => {
	let token = req.headers['x-access-token'];

	if (!token) {
		return res.status(401).json({
			success: false,
			message: 'Chưa được xác thực',
		});
	}

	jwt.verify(token, process.env.secret, (err, decoded) => {
		if (err) {
			if (err.name === 'TokenExpiredError') {
				return res.status(401).json({
					success: false,
					message: 'Phiên làm việc đã hết hạn',
				});
			}

			return res.status(500).json({
				success: false,
				message: err.message,
			});
		}

		// Check if user is exist!
		User.findById(decoded.userID).then((user) => {
			// console.log(user);
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Người dùng không tồn tại',
				});
			}

			// New access token
			let newToken = jwt.sign({ userID: decoded.userID, role: decoded.role }, process.env.secret, {
				expiresIn: process.env.tokenExpiresIn,
			});

			// Return the information including token as JSON
			return res.status(200).json({
				success: true,
				message: 'Tạo mới access token thành công',
				token: newToken,
				user: removeSensitiveUserData(user),
			});
		});
	});
});

module.exports = router;
