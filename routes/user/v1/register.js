const express = require('express');
const User = require('../../../models/user');
const router = express.Router();
const jwt = require('jsonwebtoken');

const { register_validation } = require('./utils');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const moment = require('moment');
const async = require('async');

// register with email
router.post('/email', (req, res, next) => {
	req.checkBody({
		email: {
			notEmpty: true,
			isEmail: {
				errorMessage: 'Invalid email.',
			},
			errorMessage: 'Email required.',
		},
		password: {
			notEmpty: true,
			isLength: {
				options: [ { min: 6 } ],
				errorMessage: 'Password needs at least 6 characters.',
			},
			errorMessage: 'Password required.',
		},
	});

	let errors = req.validationErrors();
	if (errors) {
		return res.status(422).send({
			success: false,
			message: 'Validation failed.',
			errors,
		});
	}

	const criticalData = {
		email: req.body.email,
		phoneNumber: req.body.phoneNumber,
	};

	let duplicates = {
		email: false,
		phoneNumber: false,
	};

	User.find({ $or: [ { email: criticalData.email }, { phoneNumber: criticalData.phoneNumber } ] })
		.then((users) => {
			if (users) {
				console.log('Found multiple users');
				users.forEach((user) => {
					console.log(`User: ${user.email} :: ${user.phoneNumber}`);
					if (user.email == criticalData.email) {
						duplicates.email = true;
					}

					if (user.phoneNumber == criticalData.phoneNumber) {
						console.log('Duplicate phone number');
						duplicates.phoneNumber = true;
					}
				});

				if (duplicates.email == true || duplicates.phoneNumber == true) {
					return res.status(409).send({
						success: false,
						message: 'Duplicate records.',
						duplicates,
					});
				} else {
					// NOTE: /register/email?role="ROLE"
					const role = req.query.role || 'User';

					let user = new User({
						email: req.body.email,
						password: req.body.password,
						name: req.body.name,
						phoneNumber: req.body.phoneNumber,
						birthDate: req.body.birthDate,
						role: role,
					});

					user.save().then((user) => {
						let token = jwt.sign({ userID: user._id, role: user.role }, process.env.secret, {
							expiresIn: process.env.tokenExpiresIn,
						});

						return res.status(201).json({
							success: true,
							token: token,
							message: 'Successfully registered.',
						});
					});
				}
			}
		})
		.catch(next);
});

router.post('/email/validate', (req, res, next) => {
	const { error } = register_validation(req.body);

	if (error) {
		return res.status(403).json({
			success: false,
			error,
		});
	}

	const criticalData = {
		email: req.body.email,
		phoneNumber: req.body.phoneNumber,
	};

	let duplicates = {
		email: false,
		phoneNumber: false,
	};

	User.find({ $or: [ { email: criticalData.email }, { phoneNumber: criticalData.phoneNumber } ] })
		.then((users) => {
			if (users.length) {
				console.log(users);
				users.forEach((user) => {
					if (user.email == criticalData.email) {
						duplicates.email = true;
					}

					if (user.phoneNumber == criticalData.phoneNumber) {
						duplicates.phoneNumber = true;
					}
				});

				return res.status(409).send({
					success: false,
					message: 'Duplicate records.',
					duplicates,
				});
			} else {
				// NOTE: /register/email?role="ROLE"
				const role = req.query.role || 'User';

				async.waterfall(
					[
						(done) => {
							crypto.randomBytes(20, function(err, buf) {
								let token = buf.toString('hex');
								done(err, token);
							});
						},
						(token, done) => {
							let user = new User({
								email: req.body.email,
								password: req.body.password,
								name: req.body.name,
								phoneNumber: req.body.phoneNumber,
								birthDate: req.body.birthDate,
								role: role,
								activated: false,
								utilToken: token,
								utilTokenExpires: moment().add(1, 'h').toDate(),
							});

							user.save(function(err) {
								done(err, token, user);
							});
						},
						(token, user, done) => {
							let smtpTransport = nodemailer.createTransport({
								service: 'gmail',
								auth: {
									user: process.env.HOST_EMAIL_ID,
									pass: process.env.HOST_EMAIL_PASSWORD,
								},
							});

							let mailOptions = {
								to: user.email,
								from: process.env.HOST_EMAIL_ID,
								subject: 'Kích hoạt tài khoản',
								text:
									'Đây là email để kích hoạt tài khoản, được gửi đến bạn từ GOVN team.\n\n' +
									'Xin nhấn vào đường dẫn dưới đây để hoàn thành đăng ký tài khoản:\n\n' +
									'https://driversystemapp.herokuapp.com/api-user/v1/register/validate?token=' +
									token +
									'\n\n' +
									'Nếu bạn không phải là người yêu cầu, xin phót lờ email này.\n',
							};

							smtpTransport.sendMail(mailOptions, function(err) {
								res.status(200).json({
									success: true,
									message:
										'Tạo tài khoản thành công, email kích hoạt đã được gửi đến địa chỉ email của bạn',
								});
								done(err, 'done');
							});
						},
					],
					(err) => {
						if (err) return next(err);
					}
				);
			}
		})
		.catch(next);
});

router.get('/validate', (req, res, next) => {
	const token = req.query.token;

	User.findOne({ utilToken: token })
		.then((user) => {
			if (!user) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy user này',
				});
			}

			user.activated = true;
			user.utilToken = undefined;
			user.utilTokenExpires = undefined;

			user.save((err) => {
				if (!err) {
					return res.status(201).redirect('https://www.go-vn.com/verify-email');
				} else {
					return res.status(400).json({
						success: false,
						err: err,
					});
				}
			});
		})
		.catch(next);
});

module.exports = router;
