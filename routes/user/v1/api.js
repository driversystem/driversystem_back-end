const express = require('express');
const app = express.Router();
const bodyParser = require('body-parser');

app.use('/register', require('./register'));
app.use('/auth', require('./auth'));

// Below APIs need an authentication
app.use('/user', require('./user'));
app.use('/report', require('./report'));
app.use('/vehicle', require('./vehicle'));
app.use('/rescue', require('./rescue'));
app.use('/group', require('./group'));
app.use('/appointment', require('./appointment'));
app.use('/stats', require('./stats'));

module.exports = app;
