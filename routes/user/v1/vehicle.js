const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Vehicle = require('../../../models/Vehicle');

// Get vehicle by ID
router.get('/:id', (req, res, next) => {
	let vehicleID = req.params.id;

	Vehicle.findById(vehicleID)
		.then((vehicle) => {
			if (!vehicle) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy xe',
				});
			}
			res.status(200).json({
				success: true,
				vehicle,
			});
		})
		.catch(next);
});

// New vehicle
router.post('/', (req, res, next) => {
	req.checkBody({
		vehicleType: {
			notEmpty: true,
		},
		vehicleModel: {
			notEmpty: true,
		},
		color: {
			notEmpty: true,
		},
		licensePlate: {
			notEmpty: true,
		},
	});

	let vehicle = new Vehicle({
		vehicleType: req.body.vehicleType,
		vehicleModel: req.body.vehicleModel,
		color: req.body.color,
		licensePlate: req.body.licensePlate,
	});

	vehicle
		.save()
		.then((newVehicle) => {
			res.status(201).json({
				success: true,
				vehicle: newVehicle,
			});
		})
		.catch(next);
});

// Update vehicle info
router.put('/:id', (req, res, next) => {
	const vehicleID = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(vehicleID)) {
		return res.status(400).json({
			success: false,
			message: 'ID của phương tiện không đúng',
		});
	}

	Vehicle.findByIdAndUpdate(vehicleID, {
		vehicleType: req.body.vehicleType != '' ? req.body.vehicleType : vehicleType,
		vehicleModel: req.body.vehicleModel != '' ? req.body.vehicleModel : vehicleModel,
		color: req.body.color != '' ? req.body.color : color,
		licensePlate: req.body.licensePlate != '' ? req.body.licensePlate : licensePlate,
	})
		.then((vehicle) => {
			if (!vehicle) {
				return res.status(400).json({
					success: false,
					message: 'Không tim thấy phương tiện',
				});
			}

			return res.status(200).json({
				success: true,
				vehicle,
			});
		})
		.catch(next);
});

// Get all vehicles
router.get('/', (req, res, next) => {
	Vehicle.find({})
		.then((vehicles) => {
			return res.status(200).json({
				success: true,
				vehicles,
			});
		})
		.catch(next);
});

module.exports = router;
