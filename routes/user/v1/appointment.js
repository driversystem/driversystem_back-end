const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Appointment = require('../../../models/Appointment');
const User = require('../../../models/user');
const Group = require('../../../models/Group');

// ultils
const { trimQuery } = require('./utils/index');

// Middlewares
const {
	authenticate,
	authorizeForAppointment: authorize,
	authorizeForAppointmentWithGroupId: authorizeWithGroupId,
	AUTHORITY,
} = require('./middleware');

// get all appointments;
router.get('/', authenticate, (req, res, next) => {
	const userRole = req.decoded.role;

	if (userRole !== 'Admin') {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	Appointment.find({})
		.then((appointments) => {
			return res.status(200).json({
				success: true,
				appointments,
			});
		})
		.catch(next);
});

// create new appointment
router.post('/', authenticate, authorizeWithGroupId, (req, res, next) => {
	let authority = req.authority;
	const userID = req.decoded.userID;

	if (!authority || authority < AUTHORITY.MODERATOR) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	let newAppointment = new Appointment({
		group: req.body.groupID,
		time: req.body.time,
		reminderTime: req.body.reminderTime,
		reminderMsg: req.body.reminderMsg,
		location: req.body.location,
		locationName: req.body.locationName,
		locationAddress: req.body.locationAddress,
		accepted: [ userID ],
		properties: req.body.properties,
		status: req.body.status,
	});

	newAppointment
		.save()
		.then((newAppointment) => {
			Group.findByIdAndUpdate(
				req.body.groupID,
				{
					$addToSet: { appointments: newAppointment._id },
				},
				{ new: true }
			).then((group) => {
				res.status(201).json({
					success: true,
					appointment: newAppointment,
				});
			});
		})
		.catch(next);
});

// Update appointment
router.put('/:id', authenticate, authorize, (req, res, next) => {
	let authority = req.authority;
	let appointmentId = req.params.id;

	if (!authority || authority < AUTHORITY.MODERATOR) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	let query = {};

	if (req.body.time) query.time = req.body.time;
	if (req.body.reminderTime) query.reminderTime = req.body.reminderTime;
	if (req.body.reminderMsg) query.reminderMsg = req.body.reminderMsg;
	if (req.body.location) query.location = req.body.location;
	if (req.body.locationName) query.locationName = req.body.locationName;
	if (req.body.properties) query.properties = req.body.properties;
	if (req.body.status) {
		query.status = req.body.status;
		if (query.status.toLowerCase() === 'ended') {
			query.endedAt = moment().toDate();
		}
	}
	// Validate id
	if (!mongoose.Types.ObjectId.isValid(appointmentId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Appointment.findByIdAndUpdate(appointmentId, query, { new: true })
		.then((appointment) => {
			if (!appointment) {
				return res.status(404).json({
					success: false,
					message: 'Appointment not found',
				});
			}
			return res.status(200).json({
				success: true,
				message: 'Appointment updated',
				appointment: appointment,
			});
		})
		.catch(next);
});

router.post('/:id/accept', authenticate, authorize, (req, res, next) => {
	const appointmentID = req.params.id;
	const userID = req.decoded.userID;
	const authority = req.authority;

	if (!authority || authority < AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn thực hiện hành động này',
		});
	}

	Appointment.findByIdAndUpdate(appointmentID, { $addToSet: { accepted: userID } }, { new: true })
		.populate({
			path: 'accepted',
			select: '_id name email',
		})
		.then((appointment) => {
			if (!appointment) {
				return res.status(404).json({
					success: false,
					message: 'không tìm thấy buổi hẹn này',
				});
			}

			User.findByIdAndUpdate(
				userID,
				{ $addToSet: { acceptedAppointments: appointmentID } },
				{ new: true }
			).then((user) => {
				return res.status(200).json({
					success: true,
					user,
					appointment,
				});
			});
		})
		.catch(next);
});

router.post('/:id/withdraw', authenticate, authorize, (req, res, next) => {
	const appointmentID = req.params.id;
	const userID = req.decoded.userID;
	const authority = req.authority;

	if (!authority || authority < AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn thực hiện hành động này',
		});
	}

	Appointment.findByIdAndUpdate(appointmentID, { $pull: { accepted: userID } }, { new: true })
		.populate({
			path: 'accepted',
			select: '_id name email',
		})
		.then((appointment) => {
			if (!appointment) {
				return res.status(404).json({
					success: false,
					message: 'không tìm thấy buổi hẹn này',
				});
			}

			User.findByIdAndUpdate(
				userID,
				{ $pull: { acceptedAppointments: appointmentID } },
				{ new: true }
			).then((user) => {
				return res.status(200).json({
					success: true,
					user,
					appointment,
				});
			});
		})
		.catch(next);
});

// Delete appointment
router.delete('/:id', authenticate, authorize, (req, res, next) => {
	let authority = req.authority;
	let appointmentId = req.params.id;

	if (!authority || authority < AUTHORITY.MODERATOR) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(appointmentId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Appointment.findById(appointmentId)
		.then((appointment) => {
			const groupId = appointment.group;
			Group.findByIdAndUpdate(
				groupId,
				{
					$pull: { appointments: appointmentId },
				},
				{ new: true }
			)
				.populate({
					path: 'appointments',
					populate: {
						path: 'accepted',
						select: '_id name email',
					},
				})
				.then((group) => {
					User.updateMany(
						{ _id: { $in: appointment.accepted } },
						{ $pull: { acceptedAppointments: appointmentId } }
					).then(() => {
						Appointment.findByIdAndDelete(appointmentId).then(() => {
							return res.status(200).json({
								success: true,
								message: 'Appointment deleted successfully',
								appointments: group.appointments,
							});
						});
					});
				});
		})
		.catch(next);
});

// Get a single appointment
router.get('/:id', authenticate, authorize, (req, res, next) => {
	let appointmentId = req.params.id;
	let authority = req.authority;

	if (!authority || authority < AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(appointmentId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Appointment.findById(appointmentId)
		.populate({
			path: 'accepted',
			select: 'name email avatar',
		})
		.populate({
			path: 'group',
			select: 'groupName _id',
		})
		.then((appointment) => {
			if (!appointment) {
				return res.status(404).json({
					success: false,
					message: 'Appointment not found',
				});
			}
			return res.status(200).json({
				success: true,
				appointment,
			});
		})
		.catch(next);
});

router.get('/:id/participants/location', authenticate, authorize, (req, res, next) => {
	const userID = req.decoded.userID;
	const appointmentId = req.params.id;
	const authority = req.authority;

	if (!authority || authority < AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(appointmentId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Appointment.findById(appointmentId)
		.populate({
			path: 'accepted',
			select: 'currentLocation socketID avatar name',
		})
		.then((appointment) => {
			if (!appointment) {
				return res.status(404).json({
					success: false,
					message: 'Appointment not found',
				});
			}

			let participants = [ ...appointment.accepted ];
			// Remove requester's own entry
			participants.splice(participants.findIndex((user) => user._id === userID), 1);

			return res.status(200).json({
				success: true,
				participants,
			});
		})
		.catch(next);
});

module.exports = router;
