const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const moment = require('moment');
const { lifetime, increaseLifetimeWithUpvote, decreaseLifetimeWithDownvote } = require('./utils');
const Report = require('../../../models/report');

// Get nearby report leads to this and output: Không tìm thấy report ??????
// // Get report by id
// router.get("/:id", (req, res, next) => {
//     let reportID = req.params.id;
//     // validate the id  -> return 404
//     if (!mongoose.Types.ObjectId.isValid(reportID)) {
//         return res.status(404).json({
//             success: false,
//             message: "Không tìm thấy report"
//         })
//     }
//     Report
//         .findById(reportID)
//         .then((report) => {
//             if (!report) {
//                 return res.status(404).json({
//                     success: false,
//                     message: "Không tìm thấy report"
//                 })
//             }
//             res.status(200).json(report);
//         }).catch(next);
// });

// Get all reports
router.get('/', (req, res, next) => {
	Report.find({})
		.then((reports) => {
			return res.status(200).json({
				success: true,
				reports,
			});
		})
		.catch(next);
});

// Update number of reports
router.put('/:id/updateNumReport', (req, res, next) => {
	let reportID = req.params.id;
	// validate the id  -> return 404
	// if (!mongoose.Types.ObjectId.isValid(reportID)) {
	//     return res.status(404).json({
	//         success: false,
	//         message: "Không tìm thấy report"
	//     })
	// }
	Report.findById(reportID, 'createdAt expiresAt type')
		.then((report) => {
			let maximumLifetime;
			// Add other types
			switch (report.type) {
				default:
					maximumLifetime = lifetime.MAX_DEFAULT;
			}

			const newExpiresAt = increaseLifetimeWithUpvote(
				report.createdAt,
				report.expiresAt,
				maximumLifetime
			);

			// Update report
			Report.findByIdAndUpdate(
				reportID,
				{
					$inc: { numReport: 1 },
					expiresAt: newExpiresAt,
				},
				{ new: true }
			)
				.then((report) => {
					return res.status(200).json({
						success: true,
						report,
					});
				})
				.catch(next);
		})
		.catch(next);
});

// Update number of delete
router.put('/:id/updateNumDelete', (req, res, next) => {
	let reportID = req.params.id;
	// validate the id  -> return 404
	// if (mongoose.Types.ObjectId.isValid(reportID)) {
	//     return res.status(404).json({
	//         success: false,
	//         message: "Không tìm thấy report"
	//     })
	// }
	Report.findById(reportID, 'createdAt expiresAt type')
		.then((report) => {
			let maximumLifetime;
			// Add other types
			switch (report.type) {
				default:
					maximumLifetime = lifetime.MAX_DEFAULT;
			}

			const newExpiresAt = decreaseLifetimeWithDownvote(
				report.createdAt,
				report.expiresAt,
				maximumLifetime
			);

			// Update report
			Report.findByIdAndUpdate(
				reportID,
				{
					$inc: { numDelete: 1 },
					expiresAt: newExpiresAt,
				},
				{ new: true }
			)
				.then((report) => {
					return res.status(200).json({
						success: true,
						report,
					});
				})
				.catch(next);
		})
		.catch(next);
});

router.post('/', (req, res, next) => {
	let expiresInMinutes;

	// TODO: Add other types
	switch (req.body.type) {
		default:
			expiresInMinutes = lifetime.DEFAULT;
	}

	let report = new Report({
		type: req.body.type,
		description: req.body.description,
		geometry: req.body.geometry,
		userID: req.body.userID,
		numReport: req.body.numReport || 1,
		numDelete: req.body.numDelete || 0,
		status: req.body.status,
		// byteAudioFile: req.body.byteAudioFile,
		// byteImageFile: req.body.byteImageFile,
		image: req.body.image,
		audio: req.body.audio,
		phoneNumber: req.body.phoneNumber,
	});

	const mergeRadius = 500; // TODO: change mergeRadius to a realistic value
	var geoOptions = {
		near: req.body.geometry,
		spherical: true,
		query: {
			type: req.body.type,
		},
		distanceField: 'distance',
		maxDistance: mergeRadius,
	};

	// Find nearby marker with same type
	Report.aggregate().near(geoOptions).then((results) => {
		if (results && results.length > 0) {
			[ newMarkerLong, newMarkerLat ] = req.body.geometry.coordinates;

			// Find closest marker in order to merge
			let markerToBeMerged = results[0];
			let maxDist = results[0].distance;
			results.forEach((result) => {
				const distanceToMarker = result.distance;
				if (distanceToMarker > maxDist) {
					maxDist = distanceToMarker;
					markerToBeMerged = result;
				}
			});

			// Merge markers (by updating existing marker's position)
			[ markerLong, markerLat ] = markerToBeMerged.geometry.coordinates;
			const newLong = (markerLong + newMarkerLong) / 2;
			const newLat = (markerLat + newMarkerLat) / 2;
			let maximumLifetime;

			// TODO: Add other types
			switch (markerToBeMerged.type) {
				default:
					maximumLifetime = lifetime.MAX_DEFAULT;
			}
			Report.findByIdAndUpdate(
				markerToBeMerged._id,
				{
					$inc: {
						numReport: 1,
					},
					geometry: {
						coordinates: [ newLong, newLat ],
					},
					expiresAt: increaseLifetimeWithUpvote(
						markerToBeMerged.createdAt,
						markerToBeMerged.expiresAt,
						maximumLifetime
					),
				},
				{ new: true }
			)
				.then((report) => {
					console.log(`Merged report into ${report._id}`);
					if (!report) {
						return res.status(404).json({
							success: false,
							message: 'Không tìm thấy report',
						});
					}
					return res.status(200).json({
						success: true,
						createdNew: false,
						report: report,
					});
				})
				.catch(next);
		} else {
			// No nearby marker of the same type found
			// in which case, add new marker
			report.expiresAt = moment().add(expiresInMinutes, 'minutes').toDate();
			report
				.save()
				.then((report) => {
					return res.status(201).json({
						success: true,
						createdNew: true,
						report: report,
					});
				})
				.catch(next);
		}
	});
});

router.get('/nearby', (req, res, next) => {
	var lat = parseFloat(req.query.lat);
	var lng = parseFloat(req.query.lng);
	var radius = parseFloat(req.query.radius) || 300; //in meters

	if ((!lng && lng !== 0) || (!lat && lat !== 0)) {
		return res.status(422).json({
			success: false,
			message: 'Yêu cầu tọa độ hiện tại với các tham số lng, lat',
		});
	}

	var geoOptions = {
		near: {
			type: 'Point',
			coordinates: [ lng, lat ],
		},
		maxDistance: radius,
		spherical: true,
		distanceField: 'distance',
	};

	console.log('Nearby Location: ' + lat + '; ' + lng);

	Report.aggregate()
		.near(geoOptions)
		.then((reports) => {
			//console.log("Time consumed: " + stats.time);
			return res.status(200).json({
				success: true,
				reports,
			});
		})
		.catch(next);
});

// Delete report
router.delete('/:id/delete', (req, res, next) => {
	let reportID = req.params.id;

	Report.findByIdAndRemove(reportID)
		.then(() => {
			return res.status(200).json({
				success: true,
				message: 'Report successfully deleted.',
			});
		})
		.catch(next);
});

module.exports = router;
