const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const RescueCenterRequest = require('../../../models/rescueCenterRequest');
const RescueTransaction = require('../../../models/rescueTransaction');

////////// Rescue center //////////
// NOTE: to add new rescue center, check /register/rescueCenter endpoint

// Get all rescue centers
router.get('/center', (req, res, next) => {
	User.find({
		role: 'RescueCenter',
	})
		.then((rescueCenters) => {
			return res.status(200).json({
				success: true,
				rescueCenters,
			});
		})
		.catch(next);
});

// Get rescue center by id
router.get('/center/:id', (req, res, next) => {
	let rescueCenterId = req.params.id;
	// validate the id  -> return 404
	if (!mongoose.Types.ObjectId.isValid(rescueCenterId)) {
		return res.status(404).json({
			success: false,
			message: 'Rescue Center not found.',
		});
	}
	User.find({
		_id: rescueCenterId,
		role: 'RescueCenter',
	})
		.then((rescueCenter) => {
			if (!rescueCenter) {
				return res.status(404).json({
					success: false,
					message: 'Rescue Center not found.',
				});
			}
			return res.status(200).json({
				success: true,
				rescueCenter,
			});
		})
		.catch(next);
});

////////// Rescue center request //////////

// Get all rescue center requests;
// Get all rescue center requests by one user by adding ?userId=<ID>
router.get('/request', (req, res, next) => {
	let byUser = req.query.userId;
	let query = {};
	if (byUser) {
		// validate the id  -> return 404
		if (!mongoose.Types.ObjectId.isValid(byUser)) {
			return res.status(404).json({
				success: false,
				message: 'User not found.',
			});
		}
		query.userID = byUser;
	}

	RescueCenterRequest.find(query)
		.then((rescueCenterRequests) => {
			return res.status(200).json({
				success: true,
				rescueCenterRequests,
			});
		})
		.catch(next);
});

// Get rescue center request by id
router.get('/request/:id', (req, res, next) => {
	let requestId = req.params.id;
	RescueCenterRequest.findById(requestId)
		.then((rescueCenterRequest) => {
			if (!rescueCenterRequest) {
				return res.status(404).json({
					success: false,
					message: 'Transaction not found.',
				});
			}
			return res.status(200).json({
				success: true,
				rescueCenterRequest,
			});
		})
		.catch(next);
});

// Create new rescue center request
router.post('/request', (req, res, next) => {
	let newRequest = new RescueCenterRequest({
		userID: req.body.userID,
		location: req.body.location,
		description: req.body.description,
		images: req.body.images,
		time: req.body.time,
		audio: req.body.audio,
		status: req.body.status,
	});
	newRequest
		.save()
		.then((newRequest) => {
			res.status(201).json({
				success: true,
				rescueCenterRequest: newRequest,
			});
		})
		.catch(next);
});

// Update rescue transaction status; add ?newStatus=<new status> to the url
router.put('/request/:id/updateStatus', (req, res, next) => {
	let newStatus = req.query.newStatus;
	let requestId = req.params.id;

	// Validate newStatus
	// TODO: create a list of valid status and check newStatus against it
	if (!newStatus) {
		return res.status(400).json({
			success: false,
			message: 'Invalid newStatus',
		});
	}

	RescueCenterRequest.findByIdAndUpdate(
		requestId,
		{
			status: newStatus,
		},
		{ new: true }
	)
		.then((rescueCenterRequest) => {
			if (!rescueCenterRequest) {
				return res.status(404).json({
					success: false,
					message: 'Transaction not found.',
				});
			}
			return res.status(200).json({
				success: true,
				rescueCenterRequest,
			});
		})
		.catch(next);
});

////////// Rescue transaction //////////

// Get all rescue transactions
// Get all rescue transactions from one rescue center by adding ?centerId=<ID>
router.get('/transaction', (req, res, next) => {
	let fromCenter = req.query.centerId;
	let query = {};
	if (fromCenter) {
		// validate the id  -> return 404
		if (!mongoose.Types.ObjectId.isValid(fromCenter)) {
			return res.status(404).json({
				success: false,
				message: 'Rescue Center not found.',
			});
		}
		query.rescueCenter = fromCenter;
	}

	RescueTransaction.find(query)
		.then((rescueTransactions) => {
			return res.status(200).json({
				success: true,
				rescueTransactions,
			});
		})
		.catch(next);
});

// Get rescue transaction by id
router.get('/transaction/:id', (req, res, next) => {
	let transactionId = req.params.id;
	RescueTransaction.findById(transactionId)
		.then((rescueTransaction) => {
			if (!rescueTransaction) {
				return res.status(404).json({
					success: false,
					message: 'Transaction not found.',
				});
			}
			return res.status(200).json({
				success: true,
				rescueTransaction,
			});
		})
		.catch(next);
});

// Create new rescue transaction
router.post('/transaction', (req, res, next) => {
	let newTransaction = new RescueTransaction({
		request: req.body.request,
		rescueCenter: req.body.rescueCenter,
		status: req.body.status,
	});
	newTransaction
		.save()
		.then((newTransaction) => {
			res.status(201).json({
				success: true,
				rescueTransaction: newTransaction,
			});
		})
		.catch(next);
});

// Update rescue transaction status; add ?newStatus=<new status> to the url
router.put('/transaction/:id/updateStatus', (req, res, next) => {
	let newStatus = req.query.newStatus;
	let transactionId = req.params.id;

	// Validate newStatus
	// TODO: create a list of valid status and check newStatus against it
	if (!newStatus) {
		return res.status(400).json({
			success: false,
			message: 'Invalid newStatus',
		});
	}

	RescueTransaction.findByIdAndUpdate(
		transactionId,
		{
			status: newStatus,
		},
		{ new: true }
	)
		.then((rescueTransaction) => {
			if (!rescueTransaction) {
				return res.status(404).json({
					success: false,
					message: 'Transaction not found.',
				});
			}
			return res.status(200).json({
				success: true,
				rescueTransaction,
			});
		})
		.catch(next);
});

module.exports = router;
