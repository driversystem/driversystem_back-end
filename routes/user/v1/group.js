const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Group = require('../../../models/Group');
const User = require('../../../models/user');

// Middlewares
const { authenticate, authorize, AUTHORITY } = require('./middleware');

// Get all groups
router.get('/', authenticate, (req, res, next) => {
	const id = req.decoded.userID;
	const role = req.decoded.role;

	if (!mongoose.Types.ObjectId.isValid(id)) {
		return res.status(400).json({
			success: false,
			message: 'User ID của token không hợp lệ',
		});
	}

	if (role != 'Admin') {
		return res.status(400).json({
			success: false,
			message: 'User không được cấp quyền để thực hiện chức năng này',
		});
	}

	Group.find({})
		.then((groups) => {
			return res.status(200).json({
				success: true,
				groups,
			});
		})
		.catch(next);
});

// Create new group
router.post('/', authenticate, (req, res, next) => {
	let newGroup = new Group({
		groupName: req.body.groupName,
		groupOwner: req.decoded.userID,
		pendingInvitations: req.body.members,
		description: req.body.description,
		avatar: req.body.avatar,
		photos: [],
	});

	newGroup
		.save()
		.then((group) => {
			User.update(
				{ _id: { $in: group.pendingInvitations } },
				{ $push: { pendingInvitations: group._id } },
				{ multi: true }
			).then(() => {
				User.findByIdAndUpdate(req.decoded.userID, {
					$push: { groups: group._id },
				}).then(() => {
					Group.populate(group, {
						path: 'groupOwner pendingInvitations',
						select: 'name email',
					}).then((group) => {
						return res.status(200).json({
							success: true,
							group,
						});
					});
				});
			});
		})
		.catch(next);
});

// Thăng chức user
router.put('/:id/moderator/add', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const newMod = req.query.id;
	// const authorization = req.authorized;
	const authority = req.authority;

	if (authority < AUTHORITY.OWNER) {
		return res.status(400).json({
			success: false,
			message: 'Người dùng không có quyền thực hiện chức nắng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findByIdAndUpdate(
		groupID,
		{
			$pull: { members: newMod },
			$addToSet: { groupModerators: newMod },
		},
		{ new: true }
	)
		.select('-appointments')
		.populate({
			path: 'groupOwner groupModerators members',
			select: 'name email _id',
		})
		.then((group) => {
			if (!group) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}
			return res.status(200).json({
				success: true,
				group: group,
			});
		})
		.catch(next);
});

// Remove moderator
router.put('/:id/moderator/remove', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const mod = req.query.id;
	// const authorization = req.authorized;
	const authority = req.authority;

	if (authority < AUTHORITY.OWNER) {
		return res.status(400).json({
			success: false,
			message: 'Người dùng không có quyền thực hiện chức nắng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	User.findByIdAndUpdate(mod, {
		$pull: { groups: groupID },
	})
		.then(() => {
			Group.findByIdAndUpdate(
				groupID,
				{
					$pull: { groupModerators: mod },
				},
				{ new: true }
			)
				.select('-appointments')
				.populate({
					path: 'groupOwner groupModerators members',
					select: 'name email _id',
				})
				.then((group) => {
					if (!group) {
						return res.status(400).json({
							success: false,
							message: 'Không tìm thấy nhóm',
						});
					}
					return res.status(200).json({
						success: true,
						group: group,
					});
				});
		})
		.catch(next);
});

// Demote from moderator to member
router.put('/:id/moderator/derole', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const mod = req.query.id;
	// const authorization = req.authorized;
	const authority = req.authority;

	if (!authority || authority < AUTHORITY.OWNER) {
		return res.status(400).json({
			success: false,
			message: 'Người dùng không có quyền thực hiện chức nắng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findByIdAndUpdate(
		groupID,
		{
			$pull: { groupModerators: mod },
			$addToSet: { members: mod },
		},
		{ new: true }
	)
		.select('-appointments')
		.populate({
			path: 'groupOwner groupModerators members',
			select: 'name email _id',
		})
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}
			return res.status(200).json({
				success: true,
				group: group,
			});
		})
		.catch(next);
});

// Gửi lời mời vào nhóm cho user khác
router.put('/:id/invite', authenticate, authorize, (req, res, next) => {
	// const authorization = req.authorized;
	const authority = req.authority;
	const groupID = req.params.id;
	const userID = req.query.id;

	if (!authority || authority <= AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	User.findByIdAndUpdate(userID, {
		$addToSet: { pendingInvitations: groupID },
	})
		.then((user) => {
			if (!user) {
				return res.status(400).json({
					success: false,
					message: 'Không tìm thấy user',
				});
			}

			Group.findByIdAndUpdate(
				groupID,
				{
					$addToSet: { pendingInvitations: userID },
				},
				{ new: true }
			)
				.select('pendingInvitations')
				.populate({
					path: 'pendingInvitations',
					select: 'name email',
				})
				.then((group) => {
					if (!group) {
						return res.status(400).json({
							success: false,
							message: 'Không tìm thấy nhóm',
						});
					}

					return res.status(200).json({
						success: true,
						message: 'Mời thành công',
						pendingInvitations: group.pendingInvitations,
					});
				});
		})
		.catch(next);
});

// Remove member
router.put('/:id/member/remove', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const memberID = req.query.id;
	// const authorization = req.authorized;
	const authority = req.authority;

	if (!authority || authority <= AUTHORITY.MEMBER) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	User.findByIdAndUpdate(memberID, {
		$pull: { groups: groupID },
	})
		.then(() => {
			Group.findByIdAndUpdate(
				groupID,
				{
					$pull: { members: memberID, groupModerators: memberID },
				},
				{ new: true }
			)
				.select('-appointments')
				.populate({
					path: 'groupOwner groupModerators members',
					select: 'name email _id avatar',
				})
				.then((group) => {
					if (!group) {
						return res.status(400).json({
							success: false,
							message: 'Không tìm thấy nhóm',
						});
					}

					return res.status(200).json({
						success: true,
						group: group,
					});
				});
		})
		.catch(next);
});

// Get group by ID
router.get('/:id', authenticate, authorize, (req, res, next) => {
	const groupID = req.params.id;
	const userID = req.decoded.userID;

	Group.findById(groupID)
		.populate({
			path: 'groupOwner groupModerators members pendingInvitations',
			select: 'name email avatar phoneNumber',
		})
		.populate({
			path: 'appointments',
			select: '_id time reminderTime reminderMsg location locationName locationAddress accepted',
			populate: {
				path: 'accepted',
				select: 'name email',
			},
		})
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}

			User.findById(userID).then((user) => {
				let favourite = false;
				if (user.favouriteGroups.includes(group._id)) {
					favourite = true;
				}

				let groupInfo = {};
				if (req.authority >= 1) {
					groupInfo = group;
					groupInfo.favourite = favourite;
				} else {
					groupInfo = {
						groupName: group.groupName,
						groupOwner: group.groupOwner,
						description: group.description,
					};
				}

				res.status(200).json({
					success: true,
					group: groupInfo,
				});
			});
		})
		.catch(next);
});

// Get group's appointments
router.get('/:id/appointments', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const authorization = req.authorized;

	if (!authorization) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findById(groupID, '_id appointments')
		.populate({
			path: 'appointments',
			populate: {
				path: 'accepted',
			},
		})
		.then((group) => {
			if (!group) {
				return res.status(400).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}
			let appointments = group.appointments;

			return res.status(200).json({
				success: true,
				appointments: appointments,
			});
		})
		.catch(next);
});

// Get group's member
router.get('/:id/members', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const authorization = req.authorized;

	if (!authorization) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findById(groupID, 'members')
		.populate({
			path: 'members',
		})
		.then((group) => {
			if (!group) {
				return res.status(400).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}

			let members = group.members;
			return res.status(200).json({
				success: true,
				members: members,
			});
		})
		.catch(next);
});

router.get('/:id/members/location', authenticate, authorize, (req, res, next) => {
	const userID = req.decoded.userID;
	const groupID = req.params.id;
	const authorization = req.authorized;

	if (!authorization) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findById(groupID, 'members')
		.populate({
			path: 'members groupModerators groupOwner',
			select: 'currentLocation socketID avatar name',
		})
		.then((group) => {
			if (!group) {
				return res.status(400).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}

			let members = [ ...group.members, ...group.groupModerators, group.groupOwner ];
			// Remove requester's own entry
			members.splice(members.findIndex((user) => user._id === userID), 1);

			return res.status(200).json({
				success: true,
				members: members,
			});
		})
		.catch(next);
});

// Get group's modetators
router.get('/:id/moderators', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const authorization = req.authorized;

	if (!authorization) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findById(groupID, 'groupModerators')
		.populate({
			path: 'groupModerators',
		})
		.then((group) => {
			if (!group) {
				return res.status(400).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}
			let moderators = group.groupModerators;

			return res.status(200).json({
				success: true,
				moderators: moderators,
			});
		})
		.catch(next);
});

// Get group's owner
router.get('/:id/owner', authenticate, authorize, (req, res, next) => {
	let groupID = req.params.id;
	const authorization = req.authorized;

	if (!authorization) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findById(groupID, 'groupOwner')
		.populate({
			path: 'groupOwner',
		})
		.then((group) => {
			if (!group) {
				return res.status(400).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}
			let owner = group.groupOwner;

			return res.status(200).json({
				success: true,
				owner: owner,
			});
		})
		.catch(next);
});

// Delete group by id
router.delete('/:id', authenticate, authorize, (req, res, next) => {
	const groupID = req.params.id;
	// const authorization = req.authorized;
	const authority = req.authority;

	if (authority < AUTHORITY.OWNER) {
		return res.status(400).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(400).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findById(groupID)
		.then((group) => {
			let users = group.members.concat(
				group.groupModerators,
				group.groupOwner,
				group.pendingInvitations
			);

			User.update(
				{ _id: { $in: users } },
				{ $pull: { groups: group._id, pendingInvitations: group._id } },
				{ multi: true }
			).then(() => {
				Group.findByIdAndDelete(groupID).then(() => {
					return res.status(200).json({
						success: true,
						message: 'Xóa thành công',
					});
				});
			});
		})
		.catch(next);
});

// Update group info
router.put('/:id', authenticate, authorize, (req, res, next) => {
	const groupID = req.params.id;
	// const authorization = req.authorized;
	const authority = req.authority;

	const patch = {};
	if (req.body.groupName) patch.groupName = req.body.groupName;
	if (req.body.description) patch.description = req.body.description;

	if (!authority || authority <= AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	if (!mongoose.Types.ObjectId.isValid(groupID)) {
		return res.status(422).json({
			success: false,
			message: 'Invalid group ID',
		});
	}

	Group.findByIdAndUpdate(groupID, patch, { new: true })
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}

			return res.status(200).json({
				success: true,
				group: group,
			});
		})
		.catch(next);
});

// Get all pending invitations
router.get('/:id/invitation', authenticate, authorize, (req, res, next) => {
	const authority = req.authority;
	const groupID = req.params.id;

	if (!authority || authority <= AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	Group.findById(groupID)
		.select('pendingInvitations')
		.populate({
			path: 'pendingInvitations',
			select: 'name email',
		})
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tim thấy nhóm',
				});
			}

			return res.status(200).json({
				success: true,
				invitations: group.pendingInvitations,
			});
		});
});

router.post('/:id/invitation/remove', authenticate, authorize, (req, res, next) => {
	const authority = req.authority;
	const invitation = req.query.invitation;
	const groupID = req.params.id;

	if (!authority || authority <= AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	Group.findByIdAndUpdate(
		groupID,
		{
			$pull: { pendingInvitations: invitation },
		},
		{ new: true }
	)
		.populate({
			path: 'pendingInvitations members groupOwner groupModerators',
			select: 'name email',
		})
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}

			User.findByIdAndUpdate(invitation, {
				$pull: { pendingInvitations: groupID },
			}).then((user) => {
				if (!user) {
					return res.status(404).json({
						success: false,
						message: 'Không tìm thấy người dùng',
					});
				}

				return res.status(200).json({
					success: true,
					group,
				});
			});
		});
});

// Rời nhóm
// /group/:id/leave?isOwner=<true:false>
router.post('/:groupID/leave', authenticate, (req, res, next) => {
	const userID = req.decoded.userID;
	const groupID = req.params.groupID;

	Group.findById(groupID)
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'không tìm thấy nhóm',
				});
			}

			if (userID == group.groupOwner) {
				const users = group.members.concat(
					group.groupModerators,
					group.groupOwner,
					group.pendingInvitations
				);

				User.updateMany(
					{ _id: { $in: users } },
					{
						$pull: { groups: groupID, pendingInvitations: groupID, favouriteGroups: groupID },
					}
				).then(() => {
					Group.findByIdAndDelete(groupID).then(() => {
						User.findById(userID).then((user) => {
							return res.status(200).json({
								success: true,
								user: user,
							});
						});
					});
				});
			} else {
				Group.findByIdAndUpdate(groupID, {
					$pull: { members: userID, groupModerators: userID },
				}).then(() => {
					User.findByIdAndUpdate(userID, {
						$pull: { groups: groupID, favouriteGroups: groupID },
					}).then((user) => {
						return res.status(200).json({
							success: true,
							user: user,
						});
					});
				});
			}
		})
		.catch(next);
});

router.post('/:id/profile/picture', authenticate, authorize, (req, res, next) => {
	const authority = req.authority;
	const groupID = req.params.id;
	const avatar = req.body.avatar;

	if (!authority || authority <= AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	Group.findByIdAndUpdate(
		groupID,
		{
			avatar: avatar,
		},
		{ new: true }
	)
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}

			return res.status(200).json({
				success: true,
				group,
			});
		})
		.catch(next);
});

// Thâm ảnh cho nhóm
router.put('/:id/photo/add', authenticate, authorize, (req, res, next) => {
	const authority = req.authority;
	const groupID = req.params.id;
	const photos = req.body.photos;

	if (!authority || authority < AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	Group.findByIdAndUpdate(
		groupID,
		{
			$addToSet: { photos: photos },
		},
		{ new: true }
	)
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}

			return res.status(200).json({
				success: true,
				group,
			});
		})
		.catch(next);
});

router.put('/:id/photo/remove', authenticate, authorize, (req, res, next) => {
	const authority = req.authority;
	const groupID = req.params.id;
	const photo = req.body.photo;

	if (!authority || authority < AUTHORITY.MEMBER) {
		return res.status(403).json({
			success: false,
			message: 'User không đủ quyền hạn để thực hiện chức năng này',
		});
	}

	Group.findByIdAndUpdate(
		groupID,
		{
			$pull: { photos: photo },
		},
		{ new: true }
	)
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy nhóm',
				});
			}

			return res.status(200).json({
				success: true,
				group,
			});
		})
		.catch(next);
});

module.exports = router;
