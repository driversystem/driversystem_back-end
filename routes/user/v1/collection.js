const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Collection = require('../../../models/collection');

// Create new collection
router.post('/', (req, res, next) => {
	let newCollection = new Collection({
		collectionName: req.body.name,
		description: req.body.description,
	});
	newCollection
		.save()
		.then((newCollection) => {
			res.status(201).json({
				success: true,
				collection: newCollection,
			});
		})
		.catch(next);
});

// Add item to collection
router.put('/:id/addLocation', (req, res, next) => {
	let collectionId = req.params.collectionId;

	if (!mongoose.Types.ObjectId.isValid(collectionId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Collection.findByIdAndUpdate(
		collectionId,
		{
			locations: locations.push(req.body.newLocation),
		},
		{ new: true }
	)
		.then((collection) => {
			if (!collection) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy bộ sưu tập',
				});
			}
			return res.status(200).json({
				success: true,
				collection,
			});
		})
		.catch(next);
});

// Update collection description
router.put('/:id/description', (req, res, next) => {
	let collectionId = req.params.collectionId;

	if (!mongoose.Types.ObjectId.isValid(collectionId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Collection.findByIdAndUpdate(
		collectionId,
		{
			description: req.body.description,
		},
		{ new: true }
	)
		.then((collection) => {
			if (!collection) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy bộ sưu tập',
				});
			}
			return res.status(200).json({
				success: true,
				collection,
			});
		})
		.catch(next);
});

// Update the name of a collection
router.put('/:id/collectionName', (req, res, next) => {
	let collectionId = req.params.collectionId;

	if (!mongoose.Types.ObjectId.isValid(collectionId)) {
		return res.status(404).json({
			success: false,
			message: 'Invalid ID',
		});
	}

	Collection.findByIdAndUpdate(
		collectionId,
		{
			collectionName: req.body.collectionName,
		},
		{ new: true }
	)
		.then((collection) => {
			if (!collection) {
				return res.status(404).json({
					success: false,
					message: 'Không tìm thấy bộ sưu tập',
				});
			}
			return res.status(200).json({
				success: true,
				collection,
			});
		})
		.catch(next);
});

// Delete collection by ID
router.delete('/:id', (req, res, next) => {
	let collectionId = req.params.collectionId;

	Collection.findByIdAndDelete(collectionId)
		.then(() => {
			return res.status(200).json({
				success: true,
				message: 'Bộ sưu tập đã được xóa.',
			});
		})
		.catch(next);
});

// Get collection by ID
router.get('/:id', (req, res, next) => {
	let collectionId = req.params.id;

	Collection.findById(collectionId)
		.then((collection) => {
			return res.status(200).json({
				success: true,
				collection: collection,
			});
		})
		.catch(next);
});
