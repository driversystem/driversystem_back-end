const User = require('../../../../models/user');
const { eachPath } = require('../../../../models/schemas/geo');
const joi = require('@hapi/joi');
const Joi = require('@hapi/joi');
const moment = require('moment');

// TODO: Add other types
// Lifetime of report types (in minutes)
const lifetime = {
	DEFAULT: 30,
	MAX_DEFAULT: 60,
};

const increaseLifetimeWithUpvote = (createdAt, expiresAt, maximumLifetime) => {
	const LIFETIME_COEFFICENT = 0.25; // variable 'a' in docs
	const AGGRESSIVENESS = 1; // variable 'b' in docs

	const c = moment(createdAt);
	const e = moment(expiresAt);
	const currentLifetime = e.diff(c);
	maximumLifetime *= 60000; // Converted to milliseconds
	const lifetimeRatio = currentLifetime / maximumLifetime;

	const lifetimeIncreaseAmount =
		LIFETIME_COEFFICENT *
		maximumLifetime *
		((1 / (lifetimeRatio * lifetimeRatio - 1 - AGGRESSIVENESS) + 1 / AGGRESSIVENESS) /
			(-1 / (1 + AGGRESSIVENESS) + 1 / AGGRESSIVENESS));

	console.log('Current lifetime: ', currentLifetime);
	console.log('Add amount: ', lifetimeIncreaseAmount);
	console.log('Before: ', e.toString());
	e.add(lifetimeIncreaseAmount, 'milliseconds');
	console.log('After: ', e.toString());

	return e.toDate();
};

const decreaseLifetimeWithDownvote = (createdAt, expiresAt, maximumLifetime) => {
	const LIFETIME_COEFFICENT = 0.25; // variable 'a' in docs
	const AGGRESSIVENESS = 1; // variable 'b' in docs
	const MINIMUM_DECREASE_COEFFICENT = 0.05;

	const c = moment(createdAt);
	const e = moment(expiresAt);
	const currentLifetime = e.diff(c);
	maximumLifetime *= 60000; // Converted to milliseconds
	const lifetimeRatio = currentLifetime / maximumLifetime;

	let decreaseCoefficient =
		(1 / (lifetimeRatio * lifetimeRatio - 1 - AGGRESSIVENESS) + 1 / AGGRESSIVENESS) /
		(-1 / (1 + AGGRESSIVENESS) + 1 / AGGRESSIVENESS);

	// Apply lower boundary
	decreaseCoefficient =
		decreaseCoefficient < MINIMUM_DECREASE_COEFFICENT
			? MINIMUM_DECREASE_COEFFICENT
			: decreaseCoefficient;

	console.log('Current lifetime: ', currentLifetime);
	console.log('Decrease coeff: ', decreaseCoefficient);

	const lifetimeDecreaseAmount = currentLifetime * LIFETIME_COEFFICENT * decreaseCoefficient;

	console.log('Subtract amount: ', lifetimeDecreaseAmount);
	console.log('Before: ', e.toString());
	e.subtract(lifetimeDecreaseAmount, 'milliseconds');
	console.log('After: ', e.toString());
	return e.toDate();
};

const removeSensitiveUserData = (user) => {
	let userObj = user.toObject ? user.toObject() : user;
	if (userObj.password) delete userObj.password;
	if (userObj.providerUID) delete userObj.providerUID;
	if (userObj.preferences) delete userObj.preferences;
	if (userObj.email_fuzzy) delete userObj.email_fuzzy;
	if (userObj.name_fuzzy) delete userObj.name_fuzzy;
	return userObj;
};

const trimQuery = (query) => {
	if (query == null) return '';
	else return query.replace('%20', ' ');
};

const dupe = (user) => {
	const email = user.email;
	const phoneNumber = user.phoneNumber;
	let dupe = {
		email: false,
		phoneNumber: false,
	};

	User.find({ $or: [ { email: email }, { phoneNumber: phoneNumber } ] }).then((users) => {
		users.forEach((user) => {
			if (user.email === email) {
				dupe.email = true;
			}

			if (user.phoneNumber === phoneNumber) {
				dupe.phoneNumber = true;
			}
		});

		return dupe;
	});
};

const register_validation = (register_data) => {
	const validationSchema = joi.object({
		email: joi
			.string()
			.email({
				minDomainSegments: 2,
				tlds: {
					allow: [ 'com', 'net' ],
				},
			})
			.required(),
		password: joi.string().min(6).max(24).required(),
		phoneNumber: joi.string().min(6),
		name: joi.string().required(),
		birthDate: joi.date(),
	});

	return validationSchema.validate(register_data);
};

const group_validation = (group_data) => {
	const validationSchema = joi.object({
		groupName: joi.string().alphanum().min(6).max(32).required(),
	});

	return validationSchema.validate(group_data);
};

module.exports = {
	lifetime,
	increaseLifetimeWithUpvote,
	decreaseLifetimeWithDownvote,
	removeSensitiveUserData,
	trimQuery,
	dupe,
	register_validation,
	group_validation,
};
