const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../../../models/user');
const Report = require('../../../models/report');

// Middlewares
const { authenticate } = require('./middleware');

router.get('/users', authenticate, async (req, res) => {
	const id = req.decoded.userID;
	const role = req.decoded.role;

	if (!mongoose.Types.ObjectId.isValid(id)) {
		return res.status(400).json({
			success: false,
			message: 'User ID của token không hợp lệ',
		});
	}

	if (role != 'Admin') {
		return res.status(400).json({
			success: false,
			message: 'User không được cấp quyền để thực hiện chức năng này',
		});
	}

	const total = await User.count();
	res.status(200).json({
		total,
	});
});

router.get('/reports', authenticate, async (req, res) => {
	const id = req.decoded.userID;
	const role = req.decoded.role;

	if (!mongoose.Types.ObjectId.isValid(id)) {
		return res.status(400).json({
			success: false,
			message: 'User ID của token không hợp lệ',
		});
	}

	if (role != 'Admin') {
		return res.status(400).json({
			success: false,
			message: 'User không được cấp quyền để thực hiện chức năng này',
		});
	}

	const active = await Report.count();
	res.status(200).json({
		active,
	});
});

module.exports = router;
