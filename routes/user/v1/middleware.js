const jwt = require('jsonwebtoken');
const Group = require('../../../models/Group');
const Appointment = require('../../../models/Appointment');

// Auth middleware
authenticate = (req, res, next) => {
	// check header or url parameters for post parameters for token
	let token = req.headers['x-access-token'];

	if (!token) {
		return res.status(401).json({
			success: false,
			message: 'Chưa được xác thực',
		});
	}

	// Decode token
	jwt.verify(token, process.env.secret, (err, decoded) => {
		if (err) {
			return res.status(401).json({
				success: false,
				message: 'Xác thực thất bại',
			});
		} else {
			// If everything is good, save to request for use in other routes
			req.decoded = decoded;
			next();
		}
	});
};

const AUTHORITY = {
	OWNER: 3,
	MODERATOR: 2,
	MEMBER: 1,
	UNAUTHORIZED: 0,
};

authorize = (req, res, next) => {
	const userID = req.decoded.userID;
	const groupID = req.params.id;
	req.authority = 0;

	Group.findById(groupID)
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Group not found.',
				});
			}

			if (group.groupOwner == userID) {
				req.authorized = 'Owner';
				req.authority = AUTHORITY.OWNER;
			} else if (group.groupModerators.includes(userID)) {
				req.authorized = 'Moderator';
				req.authority = AUTHORITY.MODERATOR;
			} else if (group.members.includes(userID)) {
				req.authorized = 'Member';
				req.authority = AUTHORITY.MEMBER;
			} else {
				req.authorized = 'Unauthorized';
				req.authority = AUTHORITY.UNAUTHORIZED;
			}

			next();
		})
		.catch(next);
};

authorizeForAppointment = (req, res, next) => {
	const userID = req.decoded.userID;
	const appointmentID = req.params.id;
	req.authority = 0;

	Appointment.findById(appointmentID)
		.select('group')
		.then((appointment) => {
			if (!appointment) {
				return res.status(404).json({
					success: false,
					message: 'Appointment not found.',
				});
			}

			const groupID = appointment.group;

			Group.findById(groupID).then((group) => {
				if (group.groupOwner == userID) {
					req.authorized = 'Owner';
					req.authority = AUTHORITY.OWNER;
				} else if (group.groupModerators.includes(userID)) {
					req.authorized = 'Moderator';
					req.authority = AUTHORITY.MODERATOR;
				} else if (group.members.includes(userID)) {
					req.authorized = 'Member';
					req.authority = AUTHORITY.MEMBER;
				} else {
					req.authorized = 'Unauthorized';
					req.authority = AUTHORITY.UNAUTHORIZED;
				}

				next();
			});
		})
		.catch(next);
};

authorizeForAppointmentWithGroupId = (req, res, next) => {
	const userID = req.decoded.userID;
	const groupID = req.body.groupID;
	Group.findById(groupID)
		.then((group) => {
			if (!group) {
				return res.status(404).json({
					success: false,
					message: 'Group not found.',
				});
			}
			if (group.groupOwner == userID) {
				req.authorized = 'Owner';
				req.authority = AUTHORITY.OWNER;
			} else if (group.groupModerators.includes(userID)) {
				req.authorized = 'Moderator';
				req.authority = AUTHORITY.MODERATOR;
			} else if (group.members.includes(userID)) {
				req.authorized = 'Member';
				req.authority = AUTHORITY.MEMBER;
			} else {
				req.authorized = 'Unauthorized';
				req.authority = AUTHORITY.UNAUTHORIZED;
			}

			next();
		})
		.catch(next);
};

module.exports = {
	authenticate,
	authorize,
	authorizeForAppointment,
	authorizeForAppointmentWithGroupId,
	AUTHORITY: AUTHORITY,
};
