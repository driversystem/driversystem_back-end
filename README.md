# Nodejs Server for Carmap

A simple nodejs server with Restful API using Express, Mongoose,..

## Installation

You need to reinstall all packages by executing: `$npm install`

Create a `.env` file in root directory

Fill out your environment configurations using `.env.example` as an example.

Execute `$npm run dev` to deploy locally

## Hosting on:

[Site](https://driversystemapp.herokuapp.com/)

## Usage

Open with editor to make changes in code if you need. Run with command line: node app-user.js

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## License

There is no license for this work. Enjoy using it
