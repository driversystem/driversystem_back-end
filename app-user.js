const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const logger = require('morgan');

const expressValidator = require('express-validator');
const cors = require('cors');

// Set up express app
const app = express();

// // Connect to mongo db
// ////////////////// LOCAL /////////////////////
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// ///////////////////// END LOCAL ///////////////////

// ////////////////// UPDATE FUZZY SEARCH INDEXES /////////////////////
// const User = require('./models/user');
// const updateFuzzy = async (Model, attrs) => {
// 	for await (const doc of Model.find()) {
// 		const obj = attrs.reduce((acc, attr) => ({ ...acc, [attr]: doc[attr] }), {});
// 		await Model.findByIdAndUpdate(doc._id, obj);
// 	}
// };
// mongoose.connection.once('open', async () => {
// 	console.log('Updating fuzzy search indexes');
// 	await updateFuzzy(User, [ 'name', 'email' ]);
// 	console.log('Fuzzy search indexes updated');
// });
// ////////////////// END UPDATE FUZZY SEARCH INDEXES /////////////////////

// Body-parser json
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

// This line must be right after any of the bodyParser middleware
app.use(expressValidator());

// Morgan to log any request to console
app.use(logger('dev'));

// Configuration
app.set('secret-jwt', process.env.secret);

// Log request middleware
app.use((req, res, next) => {
	console.log('Body: ', req.body);
	next();
});

// enable CORS from client-side
app.use(cors());

// Home page
app.get('/', (req, res) => {
	res.send('Car Map API<br>API for user: /api-user/v1/xxx');
});

// RESTful API handler
app.use('/api-user', require('./routes/user/api'));

// Error handling middlware
app.use((err, req, res, next) => {
	console.error(err);
	res.status(500).send({
		success: false,
		message: err.message,
	});
});

// Set up for socket
const http = require('http').createServer(app);
const io = require('socket.io')(http);

io.on('connection', (socket) => {
	console.log('a socket.io connection has been made');

	socket.on('chat message', (msg) => {
		// console.log('message: ' + msg);
		io.emit('chat message', msg);
	});

	socket.on('event_join_group_travel_server', (appointment_id) => {
		socket.join(`group_travel_${appointment_id}`);
	});

	socket.on('event_leave_group_travel_server', (appointment_id) => {
		socket.leave(`group_travel_${appointment_id}`);
	});

	socket.on('event_end_group_travel_server', (appointment_id) => {
		const roomName = `group_travel_${appointment_id}`;
		io.of('/').in(roomName).clients((err, socketIds) => {
			if (err) throw err;

			socketIds.forEach((socketId) => {
				io.sockets.sockets[socketId].leave(roomName);
			});
		});
	});

	socket.on('event_warn_stop_group_travel_server', (email, name, send_id, appointment_id, msg) => {
		console.log(`User ${send_id} sent to group ${appointment_id} message: ${msg}`);
		socket
			.to(`group_travel_${appointment_id}`)
			.emit('event_warn_stop_group_travel_socket', email, name, send_id, msg);
	});

	socket.on(
		'event_warn_slow_down_group_travel_server',
		(email, name, send_id, appointment_id, msg) => {
			console.log(`User ${send_id} sent to group ${appointment_id} message: ${msg}`);
			socket
				.to(`group_travel_${appointment_id}`)
				.emit('event_warn_slow_down_group_travel_socket', email, name, send_id, msg);
		}
	);

	socket.on(
		'event_warn_bad_road_condition_group_travel_server',
		(email, name, send_id, appointment_id, msg) => {
			console.log(`User ${send_id} sent to group ${appointment_id} message: ${msg}`);
			socket
				.to(`group_travel_${appointment_id}`)
				.emit('event_warn_bad_road_condition_group_travel_socket', email, name, send_id, msg);
		}
	);

	socket.on('event_hello_server', (email, name, send_id, receive_id, msg) => {
		console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + msg);
		socket.broadcast.to(receive_id).emit('event_hello_socket', email, name, send_id, msg);
	});

	socket.on('event_warn_strong_light_server', (email, name, send_id, receive_id, msg) => {
		console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + msg);
		socket.broadcast
			.to(receive_id)
			.emit('event_warn_strong_light_socket', email, name, send_id, msg);
	});

	socket.on('event_warn_watcher_server', (email, name, send_id, receive_id, msg) => {
		console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + msg);
		socket.broadcast.to(receive_id).emit('event_warn_watcher_socket', email, name, send_id, msg);
	});

	socket.on('event_warn_slow_down_server', (email, name, send_id, receive_id, msg) => {
		console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + msg);
		socket.broadcast.to(receive_id).emit('event_warn_slow_down_socket', email, name, send_id, msg);
	});

	socket.on('event_warn_turn_around_server', (email, name, send_id, receive_id, msg) => {
		console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + msg);
		socket.broadcast
			.to(receive_id)
			.emit('event_warn_turn_around_socket', email, name, send_id, msg);
	});

	socket.on('event_warn_thank_server', (email, name, send_id, receive_id, msg) => {
		console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + msg);
		socket.broadcast.to(receive_id).emit('event_warn_thank_socket', email, name, send_id, msg);
	});

	socket.on(
		'event_report_other_server',
		(email, name, send_id, receive_id, type, base64, license) => {
			console.log('user:' + send_id + 'to ' + receive_id + ' - message: ' + type);
			socket.broadcast
				.to(receive_id)
				.emit('event_report_other_socket', email, name, send_id, type, base64, license);
		}
	);

	socket.on('disconnect', () => {
		console.log('user disconnected');
	});
});

http.listen(process.env.PORT, () => {
	console.log(`listening on ${process.env.HOSTNAME}:${process.env.PORT}`);
});
